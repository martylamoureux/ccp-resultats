@extends('skeleton')

@section('body')
    <div class="app-container"><!-- START APP HEADER -->
        <div class="app-header app-header-design-dark">
            <div class="container container-boxed">
                <ul class="app-header-buttons visible-mobile">
                    <li><a href="#" class="btn btn-link btn-icon" data-navigation-horizontal-toggle="true"><span
                                    class="icon-menu"></span></a></li>
                </ul>
                <a href="index.html" class="app-header-logo app-header-logo-light app-header-logo-condensed">Project</a>
                <ul class="app-header-buttons pull-right">
                    <li><a href="#" class="btn btn-link btn-icon"><span class="icon-cog"></span></a></li>
                    <li><a href="#" class="btn btn-default">Log Out</a></li>
                </ul>
            </div>
        </div><!-- END APP HEADER  --><!-- START APP CONTENT -->
        <div class="app-content">
            <div class="app-navigation-horizontal margin-bottom-15">
                <div class="container container-boxed">
                    <nav>
                        <ul>
                            <li class="openable {{ Request::is('/') || Request::is('championnats*') || (Request::is('epreuves') && !Request::is('epreuves/*')) ? 'active' : '' }}"><a href="#"><span class="fa fa-gear"></span> Paramètrage</a>
                                <ul>
                                    <li class="{{ Request::is('/') ? 'active' : '' }}">
                                        <a href="{{ url('/') }}">
                                            <i class="fa fa-home"></i> Accueil
                                        </a>
                                    </li>
                                    <li class="{{ Request::is('championnats*') ? 'active' : '' }}">
                                        <a href="{{ route('championnats.index') }}">
                                            <i class="fa fa-trophy"></i> Championnats
                                        </a>
                                    </li>
                                    <li class="{{ Request::is('epreuves') && !Request::is('epreuves/*') ? 'active' : '' }}">
                                        <a href="{{ route('epreuves.index') }}">
                                            <i class="fa fa-calendar"></i> Épreuves
                                        </a>
                                    </li>

                                </ul>
                            @if (Request::is('epreuves*') && !Request::is('epreuves') && isset($epreuve))
                                <li class="openable active">
                                    <a href="#"><span class="fa fa-calendar"></span> {{ $epreuve }}</a>
                                    <ul>
                                        <li class="{{ Request::is('epreuves/' . $epreuve->id) ? 'active' : '' }}">
                                            <a href="{{ route('epreuves.show', $epreuve) }}">
                                                <i class="fa fa-table"></i> Grilles
                                            </a>
                                        </li>
                                    </ul>
                                @endif
                            </li>
                        </ul>
                    </nav>
                </div>
            </div><!-- START PAGE CONTAINER -->
            <div class="container container-boxed">
                @include('flashbag')
                @yield('content')
            </div>
        </div>
    </div>
@endsection