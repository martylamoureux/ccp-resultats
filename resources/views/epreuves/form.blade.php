@extends('app')
@section('title', "Épreuves")

@section('content')
    <div class="block block-condensed">
        <div class="app-heading app-heading-small">
            <div class="title">
                <h2>
                    @if ($epreuve->exists())
                        Modifier une Épreuve
                    @else
                        Nouvelle Épreuve
                    @endif
                </h2>
                </div>
        </div>
        <div class="block-content">

            {!! Form::open(['url' => URL::full()]) !!}
            <div class="form-group">
                <label class="col-md-3 control-label">
                    Nom
                </label>
                <div class="col-md-9">
                    {{ Form::text('name', old('name', $epreuve->name), ['class' => 'form-control']) }}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">
                    Date de début
                </label>
                <div class="col-md-9">
                    {{ Form::text('begin_date', old('begin_date', $epreuve->begin_date->format('d/m/Y')), ['class' => 'form-control bs-datepicker', 'placeholder' => 'JJ/MM/AAAA']) }}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-save"></i> Enregistrer
                    </button>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@endsection