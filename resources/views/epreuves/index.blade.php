@extends('app')
@section('title', "Épreuves")

@section('content')
    <div class="block block-condensed">
        <div class="app-heading app-heading-small">
            <div class="title"><h2>Épreuves</h2>
                <p>Liste des Épreuves</p></div>
            <div class="heading-elements">
                <a href="{{ route('epreuves.add') }}" class="btn btn-success btn-clean">
                    <i class="fa fa-plus"></i> Nouvelle épreuve
                </a>
            </div>
        </div>
        <div class="block-content padding-0 margin-bottom-0 table-responsive">
            <table class="table margin-bottom-0">
                <thead>
                <tr>
                    <th>Nom</th>
                    <th>Date</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($epreuves as $epreuve)
                    <tr>
                        <td>
                            <a href="{{ route('epreuves.show', $epreuve) }}" class="text-bold">
                                {{ $epreuve->name }}
                            </a>
                        </td>
                        <td>
                            {{ $epreuve->begin_date->formatLocalized('%A %d %B %Y') }}
                        </td>
                        <td>
                            <a href="{{ route('epreuves.edit', $epreuve) }}" class="btn btn-info btn-clean btn-xs">
                                <i class="fa fa-pencil"></i> Modifier
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="3" class="text-center lead text-muted">
                            Aucune épreuve
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection