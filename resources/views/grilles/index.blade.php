@extends('app')
@section('title', $epreuve)

@section('content')
    <div class="block block-condensed">
        <div class="app-heading app-heading-small">
            <div class="title">
                <h2>
                    Grilles
                </h2>
            </div>
            <div class="heading-elements">
                <a href="{{ route('grilles.add', $epreuve) }}" class="btn btn-success btn-clean">
                    <i class="fa fa-plus"></i> Nouvelle Grille
                </a>
            </div>
        </div>
        <div class="block-content padding-0 margin-bottom-0 table-responsive">
            <table class="table margin-bottom-0">
                <thead>
                <tr>
                    <th>Nom</th>
                    <th>Championnat</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($grilles as $grille)
                    <tr>
                        <td>
                            <a href="{{ route('grilles.show', [$epreuve, $grille]) }}" class="text-bold">
                                {{ $grille->name }}
                            </a>
                        </td>
                        <td>
                            {{ $grille->champopnnat->name ?? 'Aucun' }}
                        </td>
                        <td>
                            <a href="{{ route('grilles.edit', [$epreuve, $grille]) }}"
                               class="btn btn-info btn-clean btn-xs">
                                <i class="fa fa-pencil"></i> Modifier
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="3" class="text-center lead text-muted">
                            Aucune grille
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection