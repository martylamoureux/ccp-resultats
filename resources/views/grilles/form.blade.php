@extends('app')
@section('title', $epreuve)

@section('content')
    <div class="block block-condensed">
        <div class="app-heading app-heading-small">
            <div class="title">
                <h2>
                    @if ($epreuve->exists())
                        Modifier les propriétés d'une Grille
                    @else
                        Nouvelle Grille
                    @endif
                </h2>
                </div>
        </div>
        <div class="block-content">

            {!! Form::open(['url' => URL::full()]) !!}
            <div class="form-group">
                <label class="col-md-3 control-label">
                    Nom
                </label>
                <div class="col-md-9">
                    {{ Form::text('name', old('name', $grille->name), ['class' => 'form-control']) }}
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label">
                    Championnat
                </label>
                <div class="col-md-9">
                    {{ Form::select('championnat_id', $championnats, old('championnat_id', $grille->championnat_id), ['class' => 'form-control bs-select']) }}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-save"></i> Enregistrer
                    </button>
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>
@endsection