@extends('app')
@section('title', "Championnats")

@section('content')
    <div class="block block-condensed">
        <div class="app-heading app-heading-small">
            <div class="title"><h2>Championnats</h2>
                <p>Liste des Championnats</p></div>
            <div class="heading-elements">
                <a href="{{ route('championnats.add') }}" class="btn btn-success btn-clean">
                    <i class="fa fa-plus"></i> Nouveau Championnat
                </a>
            </div>
        </div>
        <div class="block-content padding-0 margin-bottom-0 table-responsive">
            <table class="table margin-bottom-0">
                <thead>
                <tr>
                    <th>Nom</th>
                    <th>Type</th>
                    <th>Actions</th>
                </tr>
                </thead>
                <tbody>
                @forelse ($championnats as $championnat)
                    <tr>
                        <td>
                            {{ $championnat->name }}
                        </td>
                        <td>
                            {{ $championnat->type_libelle }}
                        </td>
                        <td>
                            <a href="{{ route('championnats.edit', $championnat) }}" class="btn btn-info btn-clean btn-xs">
                                <i class="fa fa-pencil"></i> Modifier
                            </a>
                        </td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="3" class="text-center lead text-muted">
                            Aucun championnat
                        </td>
                    </tr>
                @endforelse
                </tbody>
            </table>
        </div>
    </div>
@endsection