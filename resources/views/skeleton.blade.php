<!DOCTYPE html>
<html lang="fr">
<head>
    <title>@yield('title') - Résultats CCP</title>

    <!-- META SECTION -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    {{--<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">--}}
    {{--<link rel="icon" href="favicon.ico" type="image/x-icon">--}}
    <!-- END META SECTION -->
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    @stack('styles')
</head>
<body>

<!-- APP WRAPPER -->
<div class="app">

    @yield('body')

</div>
<!-- END APP WRAPPER -->

<script type="text/javascript" src="{{ asset('js/vendor/jquery/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/vendor/jquery/jquery-ui.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/vendor/bootstrap/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/vendor/moment/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/vendor/bootstrap-select/bootstrap-select.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app_plugins.js') }}"></script>
@stack('scripts')
</body>
</html>