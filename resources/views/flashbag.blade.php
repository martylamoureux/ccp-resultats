@if (Session::has('success'))
    <div class="alert alert-success alert-icon-block alert-dismissible" role="alert">
        <div class="alert-icon">
            <i class="fa fa-check-circle"></i>
        </div>
        {!! Session::get('success') !!}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span class="fa fa-times"></span>
        </button>
    </div>
@endif

@if (isset($errors) && !$errors->isEmpty())
    <div class="alert alert-danger alert-icon-block" role="alert">
        <div class="alert-icon">
            <i class="fa fa-exclamation-triangle"></i>
        </div>
        @foreach ($errors->all() as $error)
            {{ $error }}<br>
        @endforeach
    </div>
@endif