<?php
use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@index');

Route::prefix('championnats')->group(function() {
	Route::get('/', 'ChampionnatsController@index')->name('championnats.index');
	Route::get('/nouveau', 'ChampionnatsController@add')->name('championnats.add');
	Route::post('/nouveau', 'ChampionnatsController@create');
	Route::get('/{championnat}/modifier', 'ChampionnatsController@edit')->name('championnats.edit');
	Route::post('/{championnat}/modifier', 'ChampionnatsController@update');
});

Route::prefix('epreuves')->group(function() {
	Route::get('/', 'EpreuvesController@index')->name('epreuves.index');
	Route::get('/nouveau', 'EpreuvesController@add')->name('epreuves.add');
	Route::post('/nouveau', 'EpreuvesController@create');
	Route::prefix('/{epreuve}')->group(function () {
		Route::get('/', 'GrillesController@index')->name('epreuves.show');
		Route::get('/nouveau', 'GrillesController@add')->name('grilles.add');
		Route::post('/nouveau', 'GrillesController@create');
		Route::get('/{grille}', 'GrillesController@show')->name('grilles.show');
		Route::get('/{grille}/modifier', 'GrillesController@edit')->name('grilles.edit');
		Route::post('/{grille}/modifier', 'GrillesController@update');
	});
	Route::get('/{epreuve}/modifier', 'EpreuvesController@edit')->name('epreuves.edit');
	Route::post('/{epreuve}/modifier', 'EpreuvesController@update');
});

