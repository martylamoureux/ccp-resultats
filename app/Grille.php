<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Grille extends Model
{
	protected $fillable = ['name', 'epreuve_id', 'championnat_id'];

	public function __toString()
	{
		return "{$this->name} - {$this->epreuve}";
	}

	public function epreuve()
	{
		return $this->belongsTo(Epreuve::class);
	}

	public function championnat()
	{
		return $this->belongsTo(Championnat::class);
	}
}
