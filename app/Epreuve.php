<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Epreuve extends Model
{
    protected $fillable = ['name', 'begin_date'];

    protected $casts = [
    	'begin_date' => 'datetime'
	];

    public function __toString()
	{
		return "{$this->name} ({$this->begin_date->format('d/m/Y')})";
	}

	public function grilles()
	{
		return $this->hasMany(Grille::class);
	}

}
