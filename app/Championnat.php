<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Championnat extends Model
{
    protected $fillable = ['name', 'type'];

    public static $types = [
    	'ccp' => 'CCP Individuel 2017',
		'lns' => 'Ligue Nationale de Speedway 2017'
	];

    public function getTypeLibelleAttribute()
	{
		return self::$types[$this->type];
	}
}
