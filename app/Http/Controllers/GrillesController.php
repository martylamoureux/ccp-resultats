<?php

namespace App\Http\Controllers;

use App\Championnat;
use App\Epreuve;
use App\Grille;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class GrillesController extends Controller
{
	public function index(Epreuve $epreuve)
	{
		$grilles = $epreuve->grilles;
		return view('grilles.index', compact('epreuve', 'grilles'));
	}

	public function add(Epreuve $epreuve)
	{
		$grille = new Grille();
		$grille->epreuve_id = $epreuve->id;

		$championnats = array_merge(['0' => 'Aucun'], Championnat::all()->pluck('name', 'id')->toArray());
		return view('grilles.form', compact('epreuve', 'grille', 'championnats'));
	}

	public function create(Epreuve $epreuve)
	{
		$this->validate(request(), [
			'name'           => 'required',
			//'championnat_id' => 'exists:championnats,id',
		]);

		$grille = new Grille(request()->all());
		$grille->epreuve_id = $epreuve->id;
		$grille->save();

		Session::flash('success', "La grille a bien été créée !");

		return redirect()->route('epreuves.show', $epreuve);
	}

	public function edit(Epreuve $epreuve, Grille $grille)
	{
		$championnats = array_merge(['0' => 'Aucun'], Championnat::all()->pluck('name', 'id')->toArray());

		return view('grilles.form', compact('grille', 'epreuve', 'championnats'));
	}

	public function update(Epreuve $epreuve, Grille $grille)
	{
		$this->validate(request(), [
			'name' => 'required',
			//'championnat_id' => 'exists:championnats,id',
		]);

		$grille->fill(request()->all());
		$grille->save();

		Session::flash('success', "La grille {$grille->name} a bien été modifiée !");

		return redirect()->route('epreuves.show', $epreuve);
	}

	public function show(Epreuve $epreuve)
	{
		return view('grilles.show', compact('epreuve'));
	}
}
