<?php

namespace App\Http\Controllers;

use App\Championnat;
use App\Epreuve;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;

class EpreuvesController extends Controller
{
    public function index()
	{
		$epreuves = Epreuve::all();
		return view('epreuves.index', compact('epreuves'));
	}

	public function add()
	{
		$epreuve = new Epreuve();
		$epreuve->begin_date = Carbon::now();
		return view('epreuves.form', compact('epreuve'));
	}

	public function create()
	{
		$this->validate(request(), [
			'name' => 'required',
			'begin_date' => 'required|date_format:d/m/Y'
		]);

		$epreuve = new Epreuve(request()->except('begin_date'));
		$epreuve->begin_date = Carbon::createFromFormat('d/m/Y', request()->get('begin_date'));
		$epreuve->save();

		Session::flash('success', "L'épreuve a bien été créée !");

		return redirect()->route('epreuves.index');
	}

	public function edit(Epreuve $epreuve)
	{
		return view('epreuves.form', compact('epreuve'));
	}

	public function update(Epreuve $epreuve)
	{
		$this->validate(request(), [
			'name'       => 'required',
			'begin_date' => 'required|date_format:d/m/Y'
		]);

		$epreuve->fill(request()->except('begin_date'));
		$epreuve->begin_date = Carbon::createFromFormat('d/m/Y', request()->get('begin_date'));
		$epreuve->save();

		Session::flash('success', "L'épreuve {$epreuve->name} a bien été modifiée !");

		return redirect()->route('epreuves.index');
	}

	public function show(Epreuve $epreuve)
	{
		return view('epreuves.show', compact('epreuve'));
	}
}
