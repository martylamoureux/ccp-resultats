<?php

namespace App\Http\Controllers;

use App\Championnat;
use Illuminate\Support\Facades\Session;

class ChampionnatsController extends Controller
{
    public function index()
	{
		$championnats = Championnat::all();
		return view('championnats.index', compact('championnats'));
	}

	public function add()
	{
		$championnat = new Championnat();
		return view('championnats.form', compact('championnat'));
	}

	public function create()
	{
		$this->validate(request(), [
			'name' => 'required',
			'type' => 'required|in:' . collect(Championnat::$types)->keys()->implode(',')
		]);

		$championnat = new Championnat(request()->all());
		$championnat->saison = 2017;

		$championnat->save();

		Session::flash('success', "Le championnat a bien été créé !");

		return redirect()->route('championnats.index');
	}

	public function edit(Championnat $championnat)
	{
		return view('championnats.form', compact('championnat'));
	}

	public function update(Championnat $championnat)
	{
		$this->validate(request(), [
			'name' => 'required',
			'type' => 'required|in:' . collect(Championnat::$types)->keys()->implode(',')
		]);

		$championnat->fill(request()->all());
		$championnat->save();

		Session::flash('success', "Le championnat {$championnat->name} a bien été modifié !");

		return redirect()->route('championnats.index');
	}
}
